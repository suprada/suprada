'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin') ;

gulp.task('sass', function(){
	return gulp.src('./src/sass/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		// .pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write('./maps'))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest('./css'));
});

gulp.task('sass-watch', function(){
	gulp.watch('./src/sass/**/.scss', ['sass']);
});

gulp.task('imagemin', function(){
	gulp.src('src/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('./images'));
})

gulp.task('image-watch', function(){
	gulp.watch('src/images/*', ['imagemin']);
})


gulp.task('default', ['sass-watch', 'image-watch']);
